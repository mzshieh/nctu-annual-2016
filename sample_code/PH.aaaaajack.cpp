#include<bits/stdc++.h>

using namespace std;

long long x[100000], y[100000];

long long gcd(long long a, long long b)
{
    if(a<0) return gcd(-a,b);
    if(b<0) return gcd(a,-b);
    if(a>b) return gcd(b,a);
    if(a==0) return b;
    return gcd(b%a,a);
}

int main()
{
    int ncases;
    scanf("%d",&ncases);
    while(ncases--)
    {
        int n;
        scanf("%d",&n);
        for(int i = 0; i < n; i++)
            scanf("%lld%lld",&x[i],&y[i]);
        long long ans = 0;
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++)
                ans = gcd(ans,x[i]*y[j]-x[j]*y[i]);
        if(ans) printf("%lld.%d\n",ans/2,(ans&1)?5:0);
		else puts("All points are colinear");
    }
	return 0;
}
