import java.io.*;
import java.util.*;

public class PC{
    
    static int n, m, e;
    static int[] ritual;
    static int[][] chart;
    
    public static void main(String[] args){
        Scan scan = new Scan();
        int testcases = scan.nextInt();
        while(testcases-- != 0){
            n = scan.nextInt();
            m = scan.nextInt();
            e = scan.nextInt();
            ritual = new int[m];
            chart = new int[m][m];
            for(int i=0;i<m;i++) Arrays.fill(chart[i], 0x3FFFFFFF);
            for(int i=0;i<m;i++) ritual[i] = scan.nextInt();
            for(int i=0;i<e;i++) chart[scan.nextInt()][scan.nextInt()] = scan.nextInt();
            floydWarshall();
            int result = dijkstra();
            System.out.println(result);
        }
    }
    
    static void floydWarshall(){
        for(int k=0;k<m;k++){
            for(int i=0;i<m;i++){
                for(int j=0;j<m;j++){
                    if(chart[i][k] + chart[k][j] < chart[i][j]) chart[i][j] = chart[i][k] + chart[k][j];
                }
            }
        }
        //System.out.println("FLOYD-WARSHALL:");
        //for(int i=0;i<m;i++){
            //for(int j=0;j<m;j++) System.out.printf("%10d ", chart[i][j]);
            //System.out.println();
        //}
        //System.out.println("END");
    }
    
    static int dijkstra(){
        int[] distance = new int[m];
        Arrays.fill(distance, Integer.MAX_VALUE);
        PriorityQueue<Entry> pq = new PriorityQueue<>();
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        for(int i=0;i<m;i++) list.add(new ArrayList<>());
        for(int i=0;i<m;i++){
            if(ritual[i] == 0){
                distance[i] = 0;
                pq.add(new Entry(i, 0));
            }
            for(int j=0;j<m;j++){
                if(chart[i][j] == 0x3FFFFFFF) continue;
                if(ritual[i]+1 == ritual[j]) list.get(i).add(j);
            }
        }
        while(!pq.isEmpty()){
            Entry now = pq.poll();
            if(distance[now.id] < now.dist) continue;
            for(int next : list.get(now.id)){
                if(distance[now.id] + chart[now.id][next] < distance[next]){
                    distance[next] = distance[now.id] + chart[now.id][next];
                    pq.add(new Entry(next, distance[next]));
                }
            }
        }
        int result = Integer.MAX_VALUE;
        for(int i=0;i<m;i++){
            if(ritual[i] != n-1) continue;
            result = Math.min(result, distance[i]);
        }
        if(result == Integer.MAX_VALUE) return -1;
        return result;
    }
    
    static class Entry implements Comparable<Entry>{
        
        int id, dist;
        
        Entry(int id, int dist){
            this.id = id;
            this.dist = dist;
        }
        
        @Override
        public int compareTo(Entry rhs){
            return dist - rhs.dist;
        }
        
    }
    
}

class Scan{
    
    BufferedReader buffer;
    StringTokenizer tok;
    
    Scan(){
        buffer = new BufferedReader(new InputStreamReader(System.in));
    }
    
    boolean hasNext(){
        while(tok==null || !tok.hasMoreElements()){
            try{
                tok = new StringTokenizer(buffer.readLine());
            }catch(Exception e){
                return false;
            }
        }
        return true;
    }
    
    String next(){
        if(hasNext()) return tok.nextToken();
        return null;
    }
    
    int nextInt(){
        return Integer.parseInt(next());
    }
    
}
