import random, sys, functools

def times(x,y):
    return x*y%1000000007

def add(x,y):
    return (x+y)%1000000007

def gen():
    n = random.randint(3,5)
    print(n)
    msg = [random.randint(0,1) for _ in range(n)]
    roots = [_ for _ in range(1,101)]
    print(' '.join(list(map(str,msg))),file=sys.stderr)
    random.shuffle(roots)
    for x in roots[:n]:
        mono = [x**p for p in range(n)]
        print(x,functools.reduce(add,map(times,mono,msg)))

print(20)
for _ in range(20):
    gen()
