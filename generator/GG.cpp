#include<bits/stdc++.h>
using namespace std;
int prime_list[5]={514229,524287,2796203,39916801,370248451};
int mul[5]={1000,1000,300,10,2};
int sp[3]={3,5,2};
int gen_even(){
	int up=1000000000,a=1;
	while(1){
		int x=sp[rand()%3];
		if(x>up/a){
			assert(a>0);
			return a;
		}
		else a*=x;
	}
}
int gen_odd(){
	int up=1000000000,a=1;
	while(1){
		int x=sp[rand()%2];
		if(x>up/a){
			assert(a>0);
			return a;
		}
		else a*=x;
	}
}
int main(){
	printf("20\n");
	printf("2\n3 15\n3\n3 5 15\n4\n3 5 15 21\n");
	for(int i=0;i<16;i++){
		printf("%d\n",10000);
		for(int j=0;j<10000;j++){
			if(j) putchar(' ');
			int ch=rand()%3,x=rand()%5;
			if(ch==0) printf("%d",prime_list[x]*(rand()%mul[x]+1));
			else if(ch==1) printf("%d",gen_odd());
			else printf("%d",gen_even());
		}
		puts("");
	}
	return 0;
}
