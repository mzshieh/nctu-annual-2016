#!/usr/bin/env python3
import random, sys

def colinear(n,x,y):
    print(n)
    pts = ['%d %d' % (_*x,_*y) for _ in range(n)]
    random.shuffle(pts)
    for _ in pts:
        print(_)

try:
    num_cases = int(sys.argv[1])
    print(num_cases)
    num_node = int(sys.argv[2])
    colinear(num_node//10,3,4)
    colinear(num_node//10,-4,3)
    colinear(num_node,0,1)
    colinear(num_node,-1,0)
    for _ in range(num_cases-4):
        n = random.randint(int(num_node*0.9),num_node)
        print(n)
        pts = set()
        while len(pts) < n:
            x, y = random.randint(-10000,10000), random.randint(-10000,10000)
            if (x,y) not in pts:
                pts.add((x,y))
        for pt in pts:
            print(pt[0], pt[1])
except:
    print('failed',file=sys.stderr)
    pass
