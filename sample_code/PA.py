ncases = int(input())
for _ in range(ncases):
    input() # pass n
    contestants = list(map(int,input().split(' ')))
    contestants.sort()
    print(sum(contestants[-3:]))
