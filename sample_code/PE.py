#!/usr/bin/env python3
import functools, operator
def rl():
    return tuple(map(int,input().split(' ')))

def solve():
    n,m,k = rl()
    a = rl()
    ans = 0
    for _ in range(m):
        u, v = rl()
        ans += a[u-1]+a[v-1]
    print(k*ans/(2*m))

for _ in range(int(input())):
    solve()
