#include<iostream>
#include<cstdio>
#include<string.h>
#include<bitset>
using namespace std;
int a[1100];
int b[1100];
long long dp[2][110000];
bitset<110000> isValid[2];
int main()
{
    int i,j,k,T;
    cin>>T;
    int n,m;
    int sum;
    while(T--)
    {
        cin>>n>>m;
        sum=0;
        for(i=0; i<n; i++)
        {
            cin>>a[i]>>b[i];
            sum+=a[i];
        }
        int maxx=0;
        int now=0;
        isValid[now].reset();
        isValid[now][0]=true;
        dp[now][0]=0;
        double ans=-1;
        for(i=0; i<n; i++)
        {
            int newMax=maxx;
            int next=!now;
            isValid[next].reset();
            for(j=0; j<=maxx; j++)
            {
                if(isValid[now][j])
                {
                    if(isValid[next][j]==true)
                    {
                        if(dp[now][j]>dp[next][j])dp[next][j]=dp[now][j];
                    }
                    else
                    {
                        isValid[next][j]=true;
                        dp[next][j]=dp[now][j];
                    }
                    int tmp=j+a[i];
                    int tmpans=dp[now][j]+b[i];
                    if(tmp<m+2000)
                    {
                        if(tmp>newMax)newMax=tmp;
                        if(tmp>=m)
                        {
                            double xans =((double)tmpans)/((double)tmp);
                            if(ans<xans){ans=xans;
                           // cout<<tmpans<<' '<<tmp<<endl;
                            }
                        }
                        if(isValid[next][tmp]==true)
                        {
                            if(tmpans>dp[next][tmp])dp[next][tmp]=tmpans;
                        }
                        else
                        {
                            // cout<<tmp<<endl;
                            isValid[next][tmp]=true;
                            dp[next][tmp]=tmpans;
                        }
                    }

                }
            }
            now=next;
            maxx=newMax;
        }
        printf("%.10f\n",ans);
    }
}
