#include<stdio.h>
#include<string.h>
long long a[110000];
long long b[110000];
int main()
{
    int T,n,m,k,i;
    long long sum;
    scanf("%d",&T);
    while(T--)
    {
        int x,y;
        memset(a,0,sizeof(a));
        scanf("%d%d%d",&n,&m,&k);
        sum=0;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&b[i]);
        }
        for(i=0;i<m;i++)
        {
            scanf("%d%d",&x,&y);
            a[x]++;
            a[y]++;
        }
        for(i=1;i<=n;i++)
        {
            sum+=a[i]*b[i];
        }
        printf("%.5f\n",((double)sum*k)/((double)(m*2)));
    }
}
