#include<bits/stdc++.h>

using namespace std;

int a[300][300];
int r[300];


void init()
{
	for(int i = 0; i < 300; i++)
		for(int j = 0; j < 300; j++)
			a[i][j] = 0x3FFFFFFF;
	for(int i = 0; i < 300; i++)
		a[i][i] = 0;
}

void print_debug(int n)
{

	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			printf("%d%c",a[i][j],j==n-1?'\n':' ');
}

int solve()
{
	int stage, n,e;
	scanf("%d%d%d",&stage,&n,&e);
	vector<pair<int,int>> ritual;
	for(int i = 0; i < n; i++)
	{
		scanf("%d",&r[i]);
		ritual.push_back(make_pair(r[i],i));
	}
	sort(ritual.begin(), ritual.end());
	for(int i = 0; i < e; i++)
	{
		int u, v, c;
		scanf("%d%d%d",&u,&v,&c);
		a[u][v]=min(a[u][v],c);
	}
	for(int i = 0; i < n; i++)
		for(int u = 0; u < n; u++)
			for(int v = 0; v < n; v++)
				a[u][v] = min(a[u][v],a[u][i]+a[i][v]);

//	print_debug(n);

	vector<int> dp(n,0x3FFFFFFF);
	for(int i = 0; i < n; i++)
	{
//printf("(r,v)=(%d,%d)\n",ritual[i].first,ritual[i].second);
		if(ritual[i].first==0)
		{
			dp[i]=0;
			//dp[i]=a[0][ritual[i].second];
			continue;
		}
		int cand = 0x3FFFFFFF;
		int v = ritual[i].second; // consider edge (u,v)
		for(int j = 0; j < i; j++)
		{
			if(ritual[j].first-ritual[i].first!=-1)
				continue;
			int u = ritual[j].second;
//printf("(u,v)=(%d,%d)\n",u,v);
			cand = min(cand,dp[j]+a[u][v]);
		}
		dp[i]=cand;
	}
//for(int i = 0; i < n; i++) printf("dp[%d]=%d\n",i,dp[i]);
	int ans = 0x3FFFFFFF;
	for(int i = 0; i < n; i++)
	{
		if(ritual[i].first==stage-1)
		{
			ans = min(ans,dp[i]);
		}
	}
	printf("%d\n",ans==0x3FFFFFFF?-1:ans);
}

int main()
{
	int ncases;
	scanf("%d",&ncases);
	while(ncases--)
	{
		init();
		solve();
	}
	return 0;
}
