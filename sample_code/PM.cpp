#include<bits/stdc++.h>
#define N 100000
using namespace std;
int len[N],tc[N],stk[N],cnt,cc;
bool vis[N],ins[N];
vector<int> g[N];
int op(int x){
	int a[5],l=0,r=0;
	for(int i=0;i<5;i++){
		a[i]=x%10;
		x/=10;
	}
	sort(a,a+5);
	for(int i=0;i<5;i++) l=l*10+a[i];
	for(int i=4;i>=0;i--) r=r*10+a[i];
	return r-l;
}
int main(){
	for(int i=0;i<N;i++){
		if(!vis[i]){
			int j;
			for(j=i;!vis[j];j=op(j)){
				vis[j]=true;
				ins[j]=true;
				stk[cnt++]=j;
			}
			if(ins[j]){
				do{
					--cnt;
					tc[stk[cnt]]=cc;
					g[cc].push_back(stk[cnt]);
					len[stk[cnt]]=g[cc].size();
					ins[stk[cnt]]=false;
				}while(stk[cnt]!=j);
				reverse(g[cc].begin(),g[cc].end());
				cc++;
			}
			for(int l=len[j]+1;cnt>0;l++){
				len[stk[--cnt]]=l;
				ins[stk[cnt]]=false;
				tc[stk[cnt]]=tc[j];
			}
		}
	}
	int T,x;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&x);
		printf("%05d\n",g[tc[x]][(999999-len[x])%g[tc[x]].size()]);
	}
	return 0;
}
