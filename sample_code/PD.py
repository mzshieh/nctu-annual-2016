import random, sys, functools

MOD = 1000000007

def read_list(_type):
    return list(map(_type,input().split(' ')))

def times(x,y):
    return x*y%MOD

def add(x,y):
    return (x+y)%MOD

def inv(x):
    return pow(x,MOD-2,MOD)

def solve():
    n = int(input())
    mat = []
    for _ in range(n):
        x, v = read_list(int)
        row = [x**p for p in range(n)]
        row.append(v)
        mat.append(row)

    for i in range(n): # clean position i
        for j in range(i,n):
            if mat[i][j]!=0:
                break
        temp = mat[i]
        mat[i] = mat[j]
        mat[j] = temp
        div = inv(mat[i][i])
        for j in range(i, n+1):
            mat[i][j] = times(mat[i][j],div)
        for j in range(0,n):
            if i==j or mat[j][i]==0:
                continue
            coef = mat[j][i]
            for k in range(i,n+1):
                mat[j][k] = add(MOD-times(mat[i][k],coef),mat[j][k])

    ans = [str(mat[i][n]) for i in range(n)]
    print(' '.join(ans))

n_times = int(input())
for _ in range(n_times):
    solve()
