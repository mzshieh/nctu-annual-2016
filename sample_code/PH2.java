import java.io.*;
import java.lang.*;
import java.util.*;
import java.math.*;

class PH2{
    public static void main(String args[]) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        for(int round = sc.nextInt(); round>0; round--)
        {
            int n = sc.nextInt();
            long x[] = new long[n];
            long y[] = new long[n];
            long ans=0L;
            for(int i = 0; i < n; i++)
            {
                x[i] = sc.nextLong();
                y[i] = sc.nextLong();
            }
            for(int i = 0; i < n; i++)
                for(int j = i+1; j < n; j++)
                    ans=BigInteger.valueOf(ans).gcd(BigInteger.valueOf(x[i]*y[j]-x[j]*y[i])).longValue();
            if(ans==0L)
                System.out.println("All points are colinear");
            else
                System.out.println(ans/2+"."+(ans%2==1L?"5":"0"));
        }
    }
}
