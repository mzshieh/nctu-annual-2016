import java.io.*;
import java.lang.*;
import java.util.*;
import java.math.*;

class PH{
    public static void main(String args[]) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        mainLoop:
        for(int round = sc.nextInt(); round>0; round--)
        {
            int n = sc.nextInt();
            
            long x = sc.nextLong(), y=sc.nextLong(), X, Y;
            if(n==1)
            {
                System.out.println("All points are colinear");
                continue mainLoop;
            }
            for(X = sc.nextLong(), Y = sc.nextLong(), n-=2; x*Y == X*y; X = sc.nextLong(), Y = sc.nextLong(), n--)
            {
                if(n==0)
                {
                    System.out.println("All points are colinear");
                    continue mainLoop;
                }
                x = BigInteger.valueOf(x).gcd(BigInteger.valueOf(X)).longValue();
                y = BigInteger.valueOf(y).gcd(BigInteger.valueOf(Y)).longValue();
            }
            
            // now we have two non-parallel vectors
            long a, b, c; // construct (a,0), (b,c) to represent the lattice
            long gcd, lcm;
            if(Y==0L)
            {
                Y = x; x = X; X = Y; // swap x, X
                Y = y; y = 0L; // swap y, Y
            }
            if(y==0L)
            {
                a = Math.abs(x);
                if(Y<0L)
                {
                    Y = -Y;
                    X = -X;
                }
                c = Y;
                b = BigInteger.valueOf(X).mod(BigInteger.valueOf(a)).longValue();
            }
            else
            {
                if(y<0L) {y=-y; x=-x;}
                if(Y<0L) {Y=-Y; X=-X;}
                gcd = BigInteger.valueOf(y).gcd(BigInteger.valueOf(Y)).longValue();
                lcm = y/gcd*Y;
                a = Math.abs((lcm/y)*x-(lcm/Y)*X);
                c = gcd;
                long yInv, YInv;
                yInv = BigInteger.valueOf(y/gcd).modInverse(BigInteger.valueOf(Y/gcd)).longValue();
                YInv = (gcd-yInv*y)/Y;
                b=BigInteger.valueOf(x*yInv+X*YInv).mod(BigInteger.valueOf(a)).longValue();
            }
            
            // Maintane the basis {(a,0), (b,c)}
            while(n-->0)
            {
                x = sc.nextLong();
                y = sc.nextLong();
                if(y<0L) {y=-y; x=-x;}
                long newA, newB, newC;
                newC = gcd = BigInteger.valueOf(y).gcd(BigInteger.valueOf(c)).longValue();
                lcm = y*(c/gcd);
                newA = BigInteger.valueOf(a).gcd(BigInteger.valueOf(x*(c/gcd)-b*(y/gcd))).longValue();
                
                long yInv, cInv;
                yInv = BigInteger.valueOf(y/gcd).modInverse(BigInteger.valueOf(c/gcd)).longValue();
                cInv = (gcd-yInv*y)/c;
                newB = BigInteger.valueOf(yInv*x+cInv*b).mod(BigInteger.valueOf(newA)).longValue();
                a = newA;
                b = newB;
                c = newC;
            }
            System.out.println(a*c/2+"."+(a*c%2==1L?"5":"0"));
        }
    }
}
