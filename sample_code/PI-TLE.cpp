#include<bits/stdc++.h>
#define N 100100
using namespace std;
vector<int> g[N];
bool col[N];
int sz[N],p[N],in[N],out[N],now;
void build(int u){
	in[u]=now++;
	for(int i=0;i<g[u].size();i++){
		build(g[u][i]);
	}
	out[u]=now;
}
int od(int u, int x, bool nc){
	if(u==x) return 0;
	int res=1;
	nc^=col[u];
	if(nc){
		for(int i=g[u].size()-1;i>=0;i--){
			int v=g[u][i];
			if(in[x]>=in[v]&&in[x]<out[v]) return res+od(v,x,nc);
			else res+=sz[v];
		}
	}
	else{
		for(int i=0;i<g[u].size();i++){
			int v=g[u][i];
			if(in[x]>=in[v]&&in[x]<out[v]) return res+od(v,x,nc);
			else res+=sz[v];
		}
	}
	return res;
}
int main(){
	int T;
	scanf("%d",&T);
	while(T--){
		int n,q,fav,u;
		char c;
		scanf("%d",&n);
		assert(n<=100000);
		fav=n-1;
		for(int i=1;i<n;i++){
			scanf("%d",&p[i]);
			assert(p[i]<i);
			g[p[i]].push_back(i);
		}
		for(int i=0;i<n;i++) sz[i]=1;
		for(int i=n-1;i>0;i--){
			sz[p[i]]+=sz[i];
		}
		now=0;
		build(0);
		printf("%d\n",od(0,fav,0));
		scanf("%d",&q);
		assert(q<=100000);
		while(q--){
			scanf(" %c%d",&c,&u);
			if(c=='a'){
				col[u]^=1;
			}
			else fav=u;
			printf("%d\n",od(0,fav,0));
		}
		for(int i=0;i<n;i++) g[i].clear(),col[i]=0;
	}
	fprintf(stderr,"%.2f s\n",1.0*clock()/CLOCKS_PER_SEC);
	return 0;
}
