#include<bits/stdc++.h>
#define N 100100
using namespace std;
int in[N],out[N],sz[N],p[N],dep[N],sum[N],lc[N];
int now;
int sl[N<<2],ss[N<<2];
bool rev[N<<2];
vector<int> g[N];
void build(int u){
	sz[u]=1;
	in[u]=now++;
	int pre=0;
	for(int i=0;i<g[u].size();i++){
		int v=g[u][i];
		dep[v]=dep[u]+1;
		lc[v]=pre;
		build(v);
		sz[u]+=sz[v];
		pre+=sz[v];
	}
	for(int i=0;i<g[u].size();i++){
		int v=g[u][i];
		sum[v]=sz[u]-1-sz[v];
	}
	out[u]=now++;
}
void build_seg(int n){
	for(int i=0;i<n;i++){
		sl[n*2+in[i]]=lc[i];
		sl[n*2+out[i]]=-lc[i];
		ss[n*2+in[i]]=sum[i];
		ss[n*2+out[i]]=-sum[i];
	}
	for(int i=n*2;i>=1;i--){
		sl[i]=sl[i<<1]+sl[i<<1|1];
		ss[i]=ss[i<<1]+ss[i<<1|1];
	}
}
inline int qnode(int u){
	return rev[u]?ss[u]-sl[u]:sl[u];
}
void rec_push(int u){
	if(u>>1){
		rec_push(u>>1);
		if(rev[u>>1]){
			rev[u]^=1;
			rev[u^1]^=1;
			sl[u>>1]=ss[u>>1]-sl[u>>1];
			rev[u>>1]=0;
		}
	}
}
void rep_pull(int u){
	while(u>>1){
		sl[u>>1]=qnode(u)+qnode(u^1);
		u>>=1;
	}
}
inline void upd(int l,int r,int n){
	l+=n*2;
	r+=n*2;
	int tl=l,tr=r+1;
	while(tl<tr){
		if(tl&1) rev[tl++]^=1;
		if(tr&1) rev[--tr]^=1;
		tl>>=1;
		tr>>=1;
	}
	rep_pull(l);
	rep_pull(r);
}
inline int qry(int l,int r,int n){
	l+=n*2;
	r+=n*2;
	int tl=l,tr=r+1,res=0;
	rec_push(l);
	rec_push(r);
	while(tl<tr){
		if(tl&1) res+=qnode(tl++);
		if(tr&1) res+=qnode(--tr);
		tl>>=1;
		tr>>=1;
	}
	return res;
}
int main(){
	int T,md;
	scanf("%d",&T);
	while(T--){
		int n,q,fav;
		char c;
		int u;
		scanf("%d",&n);
		assert(n<=100000);
		fav=n-1;
		for(int i=1;i<n;i++){
			scanf("%d",&p[i]);
			assert(p[i]<i);
			g[p[i]].push_back(i);
		}
		now=0;
		build(0);
		build_seg(n);
		printf("%d\n",dep[fav]+qry(0,in[fav],n));
		scanf("%d",&q);
		while(q--){
			scanf(" %c%d",&c,&u);
			if(c=='a'){
				if(in[u]+1<out[u]) upd(in[u]+1,out[u]-1,n);
			}
			else fav=u;
			printf("%d\n",dep[fav]+qry(0,in[fav],n));
		}
		memset(rev,0,n<<2);
		for(int i=0;i<n;i++) g[i].clear();
		/*int md=0;
		for(int i=0;i<n;i++){
			if(dep[i]>md) md=dep[i];
		}
		fprintf(stderr,"%d\n",md);*/
	}
	return 0;
}
