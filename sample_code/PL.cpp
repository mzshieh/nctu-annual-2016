#include <stdio.h>
#include <math.h>

int n;
int xx[10005], yy[10005];
double bst, bsv, tx, ty;

double calc(double x, double y)
{
	int i;
	double sum;
	sum = 0;
	for (i=0; i<n; i++)
	{
		sum += sqrt((x-xx[i])*(x-xx[i])+(y-yy[i])*(y-yy[i]));
	}
	return sum;
}

double search_x(double y, double l, double r)
{
	double p, q, pv, qv;
	while (l<=r)
	{
		p = l+(r-l)/3;
		q = r-(r-l)/3;
		pv = calc(p, y);
		qv = calc(q, y);
		if (pv <= qv)
		{
			r = q-1e-5;
			bst = p;
			bsv = pv;
		}
		else
		{
			l = p+1e-5;
			bst = q;
			bsv = qv;
		}
	}
	return bsv;
}

double search_y(double x, double l, double r)
{
	double p, q, pv, qv;
	while (l<=r)
	{
		p = l+(r-l)/3;
		q = r-(r-l)/3;
		pv = calc(x, p);
		qv = calc(x, q);
		if (pv <= qv)
		{
			r = q-1e-5;
			bst = p;
			bsv = pv;
		}
		else
		{
			l = p+1e-5;
			bst = q;
			bsv = qv;
		}
	}
}

double ab(double v)
{
	return v<0 ? -v : v;
}

int main()
{
	int count, minx, miny, maxx, maxy, i;
	double ans, res, bx, by, sx, sy;
	scanf("%d", &count);
	while(count--)
	{
		scanf("%d", &n);
		minx = 2147483647;
		maxx = -2147483647;
		miny = 2147483647;
		maxy = -2147483647;
		sx = 0;
		sy = 0;
		for (i=0; i<n; i++)
		{
			scanf("%d%d", &xx[i], &yy[i]);
			if (xx[i] < minx)
			{
				minx = xx[i];
			}
			if (xx[i] > maxx)
			{
				maxx = xx[i];
			}
			if (yy[i] < miny)
			{
				miny = yy[i];
			}
			if (yy[i] > maxy)
			{
				maxy = yy[i];
			}
			sx += xx[i];
			sy += yy[i];
		}
		ans = 1e80;
		for (i=0; i<2; i++)
		{
			if (i == 0)
			{
				tx = sx / n;
				ty = sy / n;
			}
			else
			{
				tx = (minx+maxx)/2.0;
				ty = (miny+maxy)/2.0;
			}
			while (1)
			{
				res = search_x(ty, minx, maxx);
				if (ab(res-ans) < 1e-5)
				{
					if (res < ans)
					{
						tx = bst;
						bx = tx;
						by = ty;
						ans = res;
					}
					break;
				}
				if (res < ans)
				{
					tx = bst;
					ans = res;
						bx = tx;
						by = ty;
				}
				res = search_y(tx, miny, maxy);
				if (ab(res-ans) < 1e-5)
				{
					if (res < ans)
					{
						ty = bst;
						ans = res;
						bx = tx;
						by = ty;
					}
					break;
				}
				if (res < ans)
				{
					ty = bst;
					ans = res;
						bx = tx;
						by = ty;
				}
			}
		}
		printf("%.4f %.4f\n", bx, by);
	}
	return 0;
}
