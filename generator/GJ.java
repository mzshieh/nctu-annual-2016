import java.io.*;
import java.util.*;

public class GJ{

    static final int testcases = 50, n = 50;
    static Random random;
    static Set<Point> pSet;

    public static void main(String[] args){
        random = new Random();
        System.out.println(testcases);
        for(int test=0;test<testcases;test++){
            pSet = new TreeSet<Point>();
            for(int i=0;i<n;i++){
                int x, y;
                do{
                    x = random.nextInt() % 100; 
                    y = random.nextInt() % 100;
                }while(!pSet.add(new Point(x, y)));
            }
            //System.out.println("size = "+pSet.size());
            //for(Point p : pSet) System.out.print(p.x+" "+p.y+" ");
            //System.out.println("\n");
            Point[] vertex = pSet.toArray(new Point[0]), list = new Point[vertex.length+1];
            StringBuilder sb = new StringBuilder();
            int end = convexHull(vertex, list);
            int counter = 0;
            for(int i=0;i<end-1;i++){
                sb.append(list[i].x).append(' ').append(list[i].y).append(' ');
                pSet.remove(list[i]);
                counter++;
            }
            sb.setCharAt(sb.length()-1, '\n');
            System.out.println(counter+" "+(n-counter));
            System.out.print(sb);
            sb = new StringBuilder();
            vertex = pSet.toArray(new Point[0]);
            for(int i=vertex.length-1;i>0;i--){
                int target = random.nextInt(i);
                Point temp = vertex[i];
                vertex[i] = vertex[target];
                vertex[target] = temp;
            }
            for(int i=0;i<vertex.length;i++){
                sb.append(vertex[i].x).append(' ').append(vertex[i].y).append(' ');
                pSet.remove(vertex[i]);
                counter++;
            }
            if(sb.length() > 0) sb.setCharAt(sb.length()-1, '\n');
            System.out.print(sb); 
            if(counter != n) System.out.println("!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    static int convexHull(Point[] vertex, Point[] list){
        int n = vertex.length;
        Arrays.sort(vertex);
        int index = 0;
        for(int i=0;i<n;i++){
            while(index >= 2 && ABcrossAC(list[index-2], list[index-1], vertex[i]) <= 0) index--;
            list[index++] = vertex[i];
        }
        int half_point = index+1;
        for(int i=n-2;i>=0;i--){
            while(index>=half_point && ABcrossAC(list[index-2], list[index-1], vertex[i])<=0) index--;
            list[index++] = vertex[i];
        }
        return index;
    }

    static double ABcrossAC(Point A, Point B, Point C){
        return (B.x-A.x) * (C.y-A.y) - (B.y-A.y) * (C.x-A.x);
    }

    static class Point implements Comparable<Point>{

        int x, y;

        Point(int x, int y){
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Point rhs){
            if(x == rhs.x) return y - rhs.y;
            return x-rhs.x;
        }

    }

}
