#include<bits/stdc++.h>

using namespace std;

double x[10000], y[10000];
double epsilon=1e-7;

inline double len(double X, double Y)
{
    return sqrt(X*X+Y*Y);
}

inline double cross(double x, double y, double X, double Y)
{
    return x*Y-y*X;
}

bool colinear(int n)
{
    for(int i = 2; i < n; i++)
    {
        if(fabs(cross(x[i]-x[0],y[i]-y[0],x[1]-x[0],y[1]-y[0])>0.99))
            return false;
    }
	vector<pair<double,double>> pt(n);
	for(int i = 0; i < n; i++)
		pt[i].first = x[i], pt[i].second = y[i];
	sort(pt.rbegin(),pt.rend());
	printf("%.15lf %.15lf\n",pt[n/2].first,pt[n/2].second);
    //sort(x,x+n,greater<double>());
    //sort(y,y+n,greater<double>());
    //printf("%.15lf %.15lf\n",x[n/2],y[n/2]);    
    return true;
}

void solve()
{
    int n, i;
    scanf("%d",&n);
    for(i = 0; i < n; i++)
        scanf("%lf%lf",x+i,y+i);
    if(colinear(n)) return;
    double deltaX, deltaY, ansX=0, ansY=0;
	for(int i = 0; i < n; i++)
		ansX+=x[i]*0.5, ansY+=y[i]*0.5;
    do{
        double newX=0.0, newY=0.0, norm=0.0;
        for(i = 0; i < n; i++)
        {
            double length=len(x[i]-ansX,y[i]-ansY);
            if(length==0)
            {
                norm=1.0;
                newX=x[i];
                newY=y[i];
                break;
            }
            norm+=1.0/length;
            newX+=x[i]/length;
            newY+=y[i]/length;
        }
        newX/=norm;
        newY/=norm;
        deltaX=newX-ansX;
        deltaY=newY-ansY;
        ansX=newX;
        ansY=newY;
    } while(len(deltaX,deltaY)>epsilon);
    printf("%.15f %.15f\n",ansX,ansY);
}

int main(int argc, char **argv)
{
	if(argc>1)
	{
		sscanf(argv[1],"%lf",&epsilon);
		fprintf(stderr,"%s %f\n",argv[1],epsilon);
	}
    int ncases;
    scanf("%d",&ncases);
    while(ncases--)
        solve();
    return 0;
}
