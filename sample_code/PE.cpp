#include<cstdio>
#include<iostream>
#include<string.h>
using namespace std;
long long a[110000];
long long b[110000];
int main()
{
    int T,n,m,k,i;
    long long sum;
    cin>>T;
    while(T--)
    {
        int x,y;
        memset(a,0,sizeof(a));
        cin>>n>>m>>k;
        sum=0;
        for(i=1;i<=n;i++)
        {
            cin>>b[i];
        }
        for(i=0;i<m;i++)
        {
            cin>>x>>y;
            a[x]++;
            a[y]++;
        }
        for(i=1;i<=n;i++)
        {
            sum+=a[i]*b[i];
        }
        printf("%.5f\n",((double)sum*k)/((double)(m*2)));
    }
}
