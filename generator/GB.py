import random, sys

def valid(_):
    x, y = int(_.split(' ')[0]), int(_.split(' ')[1])
    return x*y <= 1024 and x*y >= 512

f = open('GB.in.txt','rt').read().split('\n')
g = open('GB.out.txt','rt').read().split('\n')
f.pop(0)
f.pop()
g.pop()
cand = []
for _ in zip(f,g):
    if valid(_[0]):
        cand.append(_)
print(50)

random.shuffle(cand)
for _ in cand[:50]:
    x, y = _[0].split()
    if random.randint(0,1)==0:
        print(x,y)
    else:
        print(y,x)
    print(_[1],file=sys.stderr)
