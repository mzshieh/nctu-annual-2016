# NCTU Annual Programming Contest 2016

For selecting teams representing NCTU in NCPC and ACM-ICPC.

## Problem setters

+	Min-Zheng Shieh
+	Jyun-Jie Liao
+	Shang-Hsin Yu
+	Pin-Chang Pan
+	Li-Cheng Lan

## Keywords

+	A: Sort
+	B: Counting
+	C: Shortest Paths
+	D: Gauss Elimination
+	E: Probability + Graph
+	F: Dynamic programming
+	G: Nim
+	H: Lattice Canonical Form, GCD
+	I: ETT + Range operation
+	J: Delaunay triangulation (Incremental algorithm)
+	K: Geometric median (Optimization)
+	L: Exact Cover (Dancing Links)
+   M: Loop finding

## File organization

+	`testdata`
+	`latex`
+	`sample_code`
+	`generator`
+	`validator`
+	`problemset`

## How to build

+	Run `./build.sh`
	+	Run `./build.sh` under `latex/` to build the description.
	+	Run `./build.sh` under `problemset/` to build the meta files for NCTU Contest Judge.

