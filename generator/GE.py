#!/usr/bin/env python3
import sys, random

def gen():
    n = random.randint(20000,100000)
    m = random.randint(max(n,80000),100000)
    k = random.randint(10000,100000)
    print(n,m,k)
    print(' '.join([str(random.randint(1,100)) for _ in range(n)]))
    for u in range(m):
        u = random.randint(1,n)
        v = random.randint(1,n)
        while v == u:
            v = random.randint(1,n)
        print(u,v)

ncases = int(input())
print(ncases)
for _ in range(ncases):
    gen()
