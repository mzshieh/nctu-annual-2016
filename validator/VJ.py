#!/usr/bin/env python3
import sys, os, re, math

try:
    ans = open(sys.argv[1],'rt')
    out = open(sys.argv[2],'rt')
    ans = re.split('\s+',ans.read())
    out = re.split('\s+',out.read())
    if ans[-1]=='':
        ans.pop() # Pop the last empty string
    if len(out)!=0 and out[-1]=='':
        out.pop() # Pop the last empty string
    if len(ans) != len(out):
        raise
    ans = list(map(float,ans))
    out = list(map(float,out))
    pairs = zip(ans,out)
    for _ in pairs:
        if math.fabs(_[0]-_[1]) > 5.0000001e-3:
            raise
    print('AC 1.0')
except:
    print('WA 0.0')
