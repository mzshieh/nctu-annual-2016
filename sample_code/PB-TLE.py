import math, sys

def read_list(_type):
    return list(map(_type,input().split()))

def c3(n):
    return n*(n-1)*(n-2)//6

def solve():
    n, m = read_list(int)
    nm = n*m
    ans = c3(nm)-n*c3(m)-m*c3(n)
    counter = {}
    pts = []
    for _ in range(n):
        pts += [(_,y) for y in range(m)]
    for p in pts:
        for q in pts:
            if p[0]<=q[0] or p[1]==q[1]:
                continue
            dx = p[0]-q[0]
            dy = p[1]-q[1]
            g = math.gcd(dx,dy)
            a = dx // g
            b = dy // g
            c = a*p[1]-b*p[0]
            key = (a,b,c)
            counter.setdefault(key,0)
            counter[key] += 1

    for val in counter.values():
        ans -= c23[val]
    print(ans)

c23 = {}
for _ in range(101):
    c23[_*(_-1)//2]=c3(_)

ncases = read_list(int)[0]
for i in range(ncases):
    solve()
