#!/bin/bash
for i in PA PB PC PD PE PF PG PH PI PJ PK PL PM
do
	python3 each.py $i
	cd $i
	xelatex $i.tex
	rm $i.log
	rm $i.aux
	cd ..
done
xelatex main.tex
rm main.log
rm main.aux
