import java.io.*;
import java.util.*;

public class GC{
    
    final static int testcases = 70, n = 10, m = 300;
    static Random random;
    static int[][] chart;
    static int[] type;
    
    public static void main(String[] args){
        random = new Random();
        PrintWriter out = new PrintWriter(System.out);
        out.println(testcases);
        for(int test=0;test<testcases;test++){
            boolean haveSolution = (random.nextInt(2) == 1);
            if(haveSolution){
                chart = new int[m][m];
                for(int i=0;i<m;i++) Arrays.fill(chart[i], -1);
                type = new int[m];
                for(int i=0;i<n;i++) type[i] = i;
                for(int i=n;i<m;i++) type[i] = random.nextInt(n);
                Arrays.sort(type);
                for(int i=0;i<50000;i++){
                    int a = random.nextInt(m);
                    int b = random.nextInt(m);
                    if(chart[a][b] != -1) continue;
                    if(type[a]+type[b] % 2 == 0) chart[a][b] = random.nextInt(10000)+1;
                }
                while(true){
                    int a = random.nextInt(m);
                    int b = random.nextInt(m);
                    if(chart[a][b] != -1) continue;
                    if(type[a]+type[b] % 2 == 0) continue;
                    chart[a][b] = random.nextInt(10000)+1;
                    break;
                }
            }else{
                type = new int[m];
                for(int i=0;i<m;i++) type[i] = random.nextInt(n);
                chart = new int[m][m];
                for(int i=0;i<m;i++) Arrays.fill(chart[i], -1);
                for(int i=0;i<random.nextInt(90000);i++){
                    int a = random.nextInt(m);
                    int b = random.nextInt(m);
                    if(chart[a][b] != -1) continue;
                    chart[a][b] = random.nextInt(10000)+1;
                }
            }
            int[] shuffle = new int[m];
            for(int i=0;i<m;i++) shuffle[i] = i;
            for(int i=m-1;i>0;i--){
                int target = random.nextInt(i);
                shuffle[target] += shuffle[i];
                shuffle[i] = shuffle[target] - shuffle[i];
                shuffle[target] -= shuffle[i];
            }
            int[] reverse = new int[m];
            for(int i=0;i<m;i++) reverse[shuffle[i]] = i;
            int e = 0;
            for(int i=0;i<m;i++){
                for(int j=0;j<m;j++){
                    if(chart[i][j] != -1) e++;
                }
            }
            out.println(n+" "+m+" "+e);
            out.print(type[shuffle[0]]);
            for(int i=1;i<m;i++){
                out.print(" "+type[shuffle[i]]);
            }
            out.println();
            for(int i=0;i<m;i++){
                for(int j=0;j<m;j++){
                    if(chart[i][j] == -1) continue;
                    out.println(reverse[i]+" "+reverse[j]+" "+chart[i][j]);
                }
            }
        }
        out.flush();
        out.close();
    }
    
}
