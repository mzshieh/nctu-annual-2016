#!/usr/bin/env python3
import sys, os, re, math

try:
    ans = open(sys.argv[1],'rt')
    out = open(sys.argv[2],'rt')
    ans = ans.readlines()
    out = out.readlines()
except:
    print('gg')

for _ in range(int(input())):
    n = int(input())
    ans_x, ans_y = list(map(float,ans[_].split(' ')))
    out_x, out_y = list(map(float,out[_].split(' ')))
    ans_i = 0
    ans_f = 0
    out_i = 0
    out_f = 0
    for iteration in range(n):
        x, y = list(map(float,input().split(' ')))
        ans_length = ((ans_x-x)**2+(ans_y-y)**2)**0.5
        out_length = ((out_x-x)**2+(out_y-y)**2)**0.5
        ans_i += int(ans_length)
        ans_f += ans_length-int(ans_length)
        ans_i += int(ans_f)
        ans_f -= int(ans_f)
        out_i += int(out_length)
        out_f += out_length-int(out_length)
        out_i += int(out_f)
        out_f -= int(out_f)
       
    print('%.16lf' % ((ans_i-out_i)+(ans_f-out_f)))
