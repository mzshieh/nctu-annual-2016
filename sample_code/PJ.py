#!/usr/bin/env python3
import math, functools, sys
from scipy.spatial import Delaunay

def rl():
    line = input()
    if len(line):
        return list(map(int,line.split(' ')))
    return None

def angle(u,v,w):
    luv = sum([(u[i]-v[i])**2 for i in range(2)])**0.5
    luw = sum([(u[i]-w[i])**2 for i in range(2)])**0.5
    dot = sum([(u[i]-v[i])*(u[i]-w[i]) for i in range(2)])
    return (math.acos(dot/luv/luw)/math.acos(-1))*180

def angles(u,v,w):
    return angle(u,v,w),angle(v,w,u),angle(w,u,v)

def solve():
    n, m = rl()
    pt = []
    lst = rl()
    for i in range(0,2*n,2):
        pt.append((lst[i],lst[i+1]))
    lst = rl()
    for i in range(0,2*m,2):
        pt.append((lst[i],lst[i+1]))
    if n+m == 3:
        triangles = [[0,1,2]]
    else:
        triangles = Delaunay(pt).simplices
    ans = 180
    for tri in triangles:
        # print(tri,file=sys.stderr)
        a, b, c = angles(pt[tri[0]],pt[tri[1]],pt[tri[2]])
        ans = min(ans,a,b,c)

    print('%.10f'%ans)

for _ in range(int(input())):
    solve()
