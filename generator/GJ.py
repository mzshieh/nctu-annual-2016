#!/usr/bin/env python3
import sys, random

def gen():
    print(4,45)
    print(0,0,96,0,96,96,0,96)
    coor = []
    for _ in range(16,96,16):
        coor.append(str(_)+' '+str(0))
        coor.append(str(0)+' '+str(_))
        coor.append(str(_)+' '+str(96))
        coor.append(str(96)+' '+str(_))
        for __ in range(16,96,16):
            coor.append(str(_+random.randint(-1,1))+' '+str(__+random.randint(-1,1)))
    random.shuffle(coor)
    print(' '.join(coor))

ncases = int(input())
print(ncases)
for _ in range(ncases):
    gen()
